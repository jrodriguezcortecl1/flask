import pymysql

class MysqlDatabase:
    def __init__(self):
        host = "localhost"
        user = "root"
        password = "root"
        db = "datatp"
        self.con = pymysql.connect(host=host, user=user, password=password, db=db, cursorclass=pymysql.cursors.DictCursor)
        self.cur = self.con.cursor()
        print('aqui')

    def list_planned(self):
        self.cur.execute("SELECT * FROM planned LIMIT 1")
        result = self.cur.fetchall()
        return result
